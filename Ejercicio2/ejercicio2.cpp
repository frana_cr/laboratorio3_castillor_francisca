// Este es el comando para la ejecucion del programa por la terminal.
/*
 * $ g++ -o ejercicio2 ejercicio2.cpp -lpthread
 */

// Se incluyen las librerias a utilizar en el programa
#include <iostream> // Libreria estandar de C++
#include <fstream> // Libreria necesaria para abrir y cerrar archivos
#include <time.h>  // Libreria utilizada para el tiempo
#include <cstring> // Libreria manipula string y array
#include <pthread.h> // Libreria para usar las hebras

using namespace std;

// Estructura que permite pasar el archivo por parametro
struct estructuraEjercicio2 {
    ifstream archivoTexto;   // variable de tipo ifstream archivo
    int contadorLineas;       // variable de contador de las lineas
    int contadorPalabras;     // variable de contador de las palabras
    int contadorCaracteres;   // variable de contador de las caracteres
};
// Esta funcion permite realizar los calculos para contar lineas, palabras y caracteres.
void *contarElementosArchivo(void *param) {
    estructuraEjercicio2 *almacenaDatos; // estructura que recibe los datos
    almacenaDatos = (estructuraEjercicio2 *)param;  // tendra la estructura y parametro
    int contadorLineas = -1; // Contador para las lineas
    int contadorPalabras = 0; // Contador para las palabras
    int contadorCaracteres = 0; // Contador para los caracteres
    char almacenamientoLinea[3000]; // variable encargada de almacenar las lineas tiene capacidad hasta 3000
    // Recorre el archivo hasta que no se acabe
    while(!almacenaDatos->archivoTexto.eof()){
      // Se ocupa getline para leer hasta encontrar cuando termine la linea
        almacenaDatos->archivoTexto.getline(almacenamientoLinea, 3000);
        // Se crea un for para recorrer el archivo y contar las palabras
        for (int i = 0; i < strlen(almacenamientoLinea); i++){
            // el if cuenta los espacios vacios para determinar el fin de la palabra
          if ((almacenamientoLinea[i] ==  ' ' && i != 3000 && almacenamientoLinea[i+1] !=  ' ')){
            // Si se cumple la condicion aumenta el contador
            contadorPalabras++;
          }
        }
          // Fuera del for e if se realiza el calculo para contar los caracteres
          // por medio de srtlen calculando la longuitud y sumandolo con al contador
          contadorCaracteres = strlen(almacenamientoLinea) + contadorCaracteres;
          // Tambien fuera de for e if se aumenta el contador cuando se cumpla la condicion del getline
          contadorLineas++;
      }
    almacenaDatos->contadorLineas = contadorLineas;
    almacenaDatos->contadorPalabras = contadorPalabras;
    almacenaDatos->contadorCaracteres = contadorCaracteres;
    // Se imprime la cantidad de lineas palabras y caracteres segun sus contadores
    cout << " - Lineas: " << contadorLineas << endl;
    cout << " - Palabras: " << contadorPalabras << endl;
    cout << " - Caracteres: " << contadorCaracteres << endl;
    //cout << endl;
  // Termina la hevra y hace que los valores puedan estar disponibles
    pthread_exit(0);
}

// Se crea esta funcion para calcular el tiempo de ejecucion del programa
float tiempoEjecucion(float tiempoInicial, float tiempoFinal) {
    // El tiempo de ejecucion es igual a la diferencia de tiempo final e incial
    // CLOCKS_PER_SEC permite obtener la cantidad de segundos
    float tiempoDiferencia = (tiempoFinal - tiempoInicial)/CLOCKS_PER_SEC;
    return tiempoDiferencia;
}

// El main permite la ejecucion del programa e implementacion de funciones
int main (int argc, char* argv[]) {
  // Si el argc es mayor o igual a uno se ingresan los archivos de texto
    if (argc >= 1) {
        cout << "Ingresando archivos de texto ..." << endl;
        cout << endl;
    }
    // se define el tamaño de la hebra por cada archivo hay 1 funcion que hace los 3 procesos cuenta las letras, palabras y caracteres
    pthread_t threads[argc*1-1];
    estructuraEjercicio2 informacionContenido; // variable que contiene la estructura y archivo
    float tiempoInicialEjecucion, tiempoFinalEjecucion; // variables foat del tiempo final e incial
    int contadorTotalLineas = 0; // contador total de lineas
    int contadorTotalPalabras = 0; // contador total de palabras
    int contadorTotalCaracteres = 0; // contador total de CARACTERES
    // Se imprime un mensaje que mostrara la informacion de cada archivo
    cout << " ------------------------------------------ " << endl;
    cout << " INFORMACION DE CADA ARCHIVO " << endl;
    cout << " ------------------------------------------ " << endl;

    tiempoInicialEjecucion = clock(); // se inicia el tiempo incial en el reloj
    // Se hace un for para poder realizar la operacion de las hebras y el resumen total
    for (int i=1; i < argc ; i++) {
      // se imprime el nombre del archivo
        cout << " NOMBRE ARCHIVO: " << argv[i] << endl;
        // Se abrira el archivo
        informacionContenido.archivoTexto.open(argv[i]);
        //Se creara la hebra, pasamos el arreglo, NULL, la funcion y el parametro
        pthread_create(&threads[i-1], NULL, contarElementosArchivo, (void *)&informacionContenido);
        // esperamos el termino de todos los hilos
        pthread_join(threads[i-1], NULL);
        // El archivo se cerrara
        informacionContenido.archivoTexto.close();
        // Contador total de lineas + contenido de las lineas por separado de los archivos
        contadorTotalLineas = contadorTotalLineas + informacionContenido.contadorLineas;
        // Contador total de palabras + contenido de las palabras por separado de los archivos
        contadorTotalPalabras = contadorTotalPalabras + informacionContenido.contadorPalabras;
        // Contador total de caracteres + contenido de caracteres por separado de los archivos
        contadorTotalCaracteres = contadorTotalCaracteres + informacionContenido.contadorCaracteres;
    }
        // se termina el tiempo final
        tiempoFinalEjecucion = clock();
    // Impresion del resumen total de lineas palabras y caracteres
    cout << endl;
    cout << " ------------------------------------------ " << endl;
    cout << " RESUMEN TOTAL " << endl;
    cout << " ------------------------------------------ " << endl;
    cout << " - Total Lineas: " << contadorTotalLineas << endl;
    cout << " - Total Palabras: " <<  contadorTotalPalabras << endl;
    cout << " - Total Caracteres: "  <<  contadorTotalCaracteres << endl;
    cout << endl;
    // Impresion que mostrara el tiempo de ejecucion del programa en segundos
    cout << " ------------------------------------------ " << endl;
    cout << " TIEMPO DE EJECUCION PROGRAMA: " << endl;
    cout << " ------------------------------------------ " << endl;
    cout << tiempoEjecucion(tiempoInicialEjecucion, tiempoFinalEjecucion) << " SEG" << endl;
    return 0;
}
