# Laboratorio3_CastilloR_Francisca

Laboratorio 3, Unidad 2 : " Hebras"

## Comenzando
### --- EXPLICACION PROBLEMATICA GENERAL ---
La problematica del tercer laboratorio de la unidad 2 , es que se cree un programa que permita contar las lineas, palabras y caracteres de un archivo mostrando el resumen del total de las lineas, palabras y caracteres que tengan los archivos, tambien luego se tiene que generar otro programa en donde se cree un thread por cada archivo que cuenta realizando la misma funcion (lo que significa que cuente las lineas, palabras y caracteres) y se tiene que obtener igualmente el total de la suma de todo (RESUMEN), las funciones en este caso pueden ser hebras.
Finalmente hay que medir el tiempo que se emplea en la version secuencia de contar cuanto los archivos tardan tanto el primero como el segundo para poder ver lo que se demoran en segundos.

### ---- COMO SE DESARROLLO ----

Para poder resolver la problematica de la guia lo primero que se intenta hacer es poder comprender como buscar un metodo para poder calcular los caracteres, palabras y lineas. Como se planteaba en el laboratorio se desarrollo un primer programa con el nombre de ejercicio1 (el cual esta en una carpeta con su nombre) en donde se calculo la cantidad de caracteres, palabras y lineas sin utilizar hebras; Ademas en la parte final del programa se muestra el tiempo de ejecucion que tarda en realizar el proceso para poder establecer una comparativa y ver cual ejercicio tarda menor tiempo si el de hebras o el que trabaja sin ellas. Los archivos para solucionar la problematica son: 

1. Makefile:  Permite que se pueda compilar el codigo mediante la terminal y desarrollar las pruebas de ejecucion en donde se indican los posibles errores del codigo y a la vez indica si la compilacion se ha desarrollado de manera exitosa. Es un requerimiento imprescindible dentro de los programas para poder ver su funcionalidad.

2. ejercicio1 : Este es el programa principal en donde se desarrolla la funcion que permite realizar los calculos y tambien la funcion que permite que se calcule el timepo de ejecucion del programa. Lo que contiene este programa es lo siguiente:
    - contarElementosArchivo: Esta funcion principal permite realizar el calculo de los elementos de cada uno de los archivo (de manera individual) los cuales son:
        1. Los caracteres : por medio de strlen para obtener los caracteres y luego sumarlos y aumentar el contador para luego imprimirlo y mostarlo para que el usuario pueda saber los caracteres que hay en cada archivo.
        2. Las lineas: por medio de getline se puede obtener la cantidad de lineas que posee el archivo y luego se almacena en la variable almacenamientoLinea.Cada vez que encuente una coincidencia se aumenta el contador y se almacena en la variable con el mismo nombre para luego imprimir la cantidad lineas de cada archivo.
        3. Las palabras: por medio de un while y un if se filtran para poder obtener las palabras luego de cada termino de palabra por medio del espacio vacio. Cada vez que encuente una coincidencia se aumenta el contador y se almacena en la variable con el mismo nombre para luego imprimir la cantidad de palabras de cada archivo.
    - tiempoEjecucion: En esta funcion se calcula el tiempo de ejecucion que se demora el programa, esto se realiza restando el tiempo final e inicial y luego dividiendo por CLOCKS_PER_SEC de esta manera obtenemos el tiempo total (la diferencia de tiempo) en segundos para que el usuario luego interprete cual programa si el 1 o 2 se realiza mas rapido.
    - Main : Elemento necesario para que se pueda ejecutar el programa y este pueda funcionar de manera optima. En este caso se realiza una validacion dependiendo la cantidad de parametros que ingrese el usuario si estos son mayor o igual a 1 se dice que se comenzara a leer el archivo. Como todo se realizo dentro de una misma funcion se genero un array en la funcion _contarElementosArchivo_ el cual tiene posiciones especificos dependiendo si es caracteres [2], palabras [1], lineas [0]. Por lo tanto lo primero que se declara es el array  debido a que con este resultado en la posicion del array se puede obtener la cantidad total de las lineas, plaabras y caracteres de todos los archivos y no de manera individual como lo que realiza la funcion _contarElementosArchivo_. Se imprime un mensaje al usuario en donde se muestra la informacion de cada archivo y tambien el nombre del archivo que el usuario ingrese. 
     Posteriormente se da inicio al contador del tiempo inicial, se abre el archivo y se realizan los calculos totales de todas las lineas, palabras y caracteres, para esto se llaman a las posiciones del array segun sea lo requerido mostra, luego el contador del tiempo final es agregado y el archivo cerrado.
     Tambien luego se imprime un mensaje que mostrara la informacion de resumen de los archivos que ingrese el usuario mostrando la cantidad total (suma) de las lineas, palabras y caractes , tambien se imprime el mensaje para que el usuario vea el tiempo de ejecucion del programa.

Otra opcion o solucion para poder realizar el tercer apartado de la guia que es medir el tiempo de cada una de las versiones se tiene que realizar lo siguiente:
- Por medio de la terminal al momento de realizar la compilacion del cada uno de los archivos se tienen que ingresar de la siguiente manera.    
    - _usr/bin/time -f %e + ./ejercicio1 + nombre_archivo_ 
     
     Esto se realiza de la siguiente manera para poder establecer y medir cual es el tiempo en segundos que se demora el programa en funcionar y realizar su funcion. 

 - -f = es para poder ver el formato de salida del entorno de TIME
 - %e = por medio de este parametro podemos ver la forma de salida del tiempo en este caso se quiere medir en segundos.
     Estos parametros pueden variar segun las necesidades que desea el usuario para mayor informacion : https://man7.org/linux/man-pages/man1/time.1.html 

Luego de que el usuario compruebe su funcionamiento por medio de la terminal ./ejercicio1 + nombre_archivo se concluye que el programa se ejecuta con exito sin problematicas, la unica discrepancia que tiene es que el programa cuenta de mas caracteres especiales por ejemplo palabras con tildes o con ñ agrega un caracter de mas, tambien se agrega en las palabras muchas veces no cuenta las que poseen menos de 1 caracter. Se cuenta bien las lineas, palabras y caracteres y tambien se muestra el tiempo que se demora el programa.

## Prerequisitos

- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (atom o vim)
- Time (medir tiempo del trabajo)
- G++ (para compilar)


## Instalacion

1. Para poder ejecutar los programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando:

- _lsb_release -a (versión de Ubuntu)_

_En donde:_ Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible   con la aplicación que se está desarrollando.


2. Para descargar un editor de texto como vim se puede descargar de la siguiente manera:

- _sudo apt install vim_

_En donde:_ Por medio de este editor de texto se puede construir el codigo.
Tambien si se desea ocupar un editor de texto diferente a vim este se puede instalar por la terminal o por el centro de software en donde se escribe el editor que se desea conseguir y luego se ejecuta la descarga.

En el caso del desarrollo del trabajo se implemento el editor de texto Atom, descargado de Ubuntu Software.

3. Para poder medir el tiempo que se demora el funcionamiento del trabajo se tiene que realizar de la siguiente manera para poder instalar o corroborar que la herramienta esta en el sistema operativo (Linux en este caso).

    - _sudo apt-get update time _

     _En donde:_ Permite actualizar la lista de los paquetes que hay en la computadora con sus respectivas de versiones, se ejecuta en caso de que time ya estuviera instalado previamente para que se actualice a su version mas reciente de esta manera se pueden disminuir errores con su ejecucion.

    - _sudo apt-get install time_

     _En donde:_ Se instala de manera directa y rapida por medio de la terminal el programa, lo unico que se requiere luego de ingresar el comando es agregar la contraseña del usuario y confirmar la instalacion con la letra S que puede estar en minuscula como mayuscula.


4. INSTALACION MAKEFILE
Para poder llevar a cabo el funcionamiento del programa se requiere la instalacion del Make para esto primero hay que revisar que este en la computadora, para corroborar se debe ejecutar este comando:

- _make_

De esta forma se puede ver si esta instalado para luego llevar a cabo el desarrollo del programa, si este no esta instalado, se tiene que insertar el siguiente comando en la terminal de la carpeta del programa.

- _sudo apt install make_

_En donde:_ se procede a la creacion del make para que luego este se lleve a cabo y se pueda observar la construccion del archivo.
Luego de instalar el Make, para poder ejecutar las pruebas se tiene que en la terminal entrar en la carpeta donde se encuentran los archivos mediante el comando cd con el nombre de dicha carpeta, luego ver si estan los archivos correctos o vizualizar lo que contiene la carpeta se debe ingresar el comando ls, finalmente se debe escribir make en la terminal con el siguiente comando:

- _make_

## Ejecutando Pruebas
Cada archivo se puede ejecutar para ver si tiene errores por medio del comando en terminal g++ Nombre del archivo.cpp -o Nombre Archivo. o por medio de un archivo Makefile donde se reunen las condiciones para hacer funcionar el programa
Para poder ejecutar el programa se tiene que hacer las siguientes condiciones:
Se debe ingresar a la carpeta donde se esta trabajando con los archivos del programa y esta debe contener el archivo Makefile, si este no ha sido probado con anterioridad luego se agrega el comando:

- _make_

De esta manera se esta compilando el trabajo, si este no presenta comentarios significa que la prueba finalizo con exito.
Por otra parte si se esta probando con el comando G++ se tiene que hacer el siguiente comando por terminal:

- _g++ -o ejercicio1 ejercicio1.cpp_

Si la carpeta con archivos presenta otras extensiones diferentes a .cpp, .h o al archivo makefile se debe ejecutar el siguiente comando:

- _make clean_

Posteriormete se ejecuta en la terminal

- _make_

Luego se lleva a cabo el programa ejecutando el siguiente comando por terminal:

- _./ejercicio1 + archivo_ 

##Construido con:

- Ubuntu: Sistema operativo.
- C++: Lenguaje de programación.
- Atom: Editor de código.
- G++: Compilador ocupado en C++


## Versiones

Versiones de herramientas:
- Ubuntu 20.04 LTS
- Atom 1.57.0
Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/laboratorio2_castillor_francisca

## Autores
Francisca Castillo - Desarrollo del código y proyecto, narración README.

## Expresiones de gratitud
A los ejemplos en la plataforma de Educandus de Alejandro Valdes: https://lms.educandus.cl/mod/lesson/view.php?id=569965
Tambien al ayudante del modulo por aclaracion de dudas y a la informacion obtenida algunas paginas web que se usaron para guiar el proceso las cuales son:
http://progra.usm.cl/apunte/c/contar-palabras.html
https://www.lawebdelprogramador.com/foros/Dev-C/1641695-Saber-cuantas-palabras-hay-en-un-string-o-cadena.html
https://www.ajpdsoft.com/modules.php?name=Content&pa=showpage&pid=239
https://aprenderaprogramar.com/foros/index.php?topic=930.0
https://foro.elhacker.net/programacion_cc/contar_nuemero_de_lineas_de_un_fichero-t412884.0.html
https://es.stackoverflow.com/questions/390085/c%C3%B3mo-contar-l%C3%ADneas-de-un-archivo-en-c
