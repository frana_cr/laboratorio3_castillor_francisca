// Este es el comando para la ejecucion del programa por la terminal.
/*
 * $ g++ ejercicio1.cpp -o ejercicio1
 */

// Se incluyen las librerias a utilizar en el programa
#include <iostream> // Libreria estandar de C++
#include <fstream> // Libreria necesaria para abrir y cerrar archivos
#include <time.h>  // Libreria utilizada para el tiempo
#include <cstring> // Libreria manipula string y array

// Por medio de esta opcion sacamos los std:: del cout y endl.
using namespace std;

// Funcion que desarrolla los calculos de las lineas, palabras y caracteres
int* contarElementosArchivo(ifstream &archivo) {
  // se hace un array estatico de 3 para luego obtener las lineas, palabras y caracteres por separado
    static int array[3];
    int contadorLineas = -1; // Contador para las lineas
    int contadorPalabras = 0; // Contador de palabras
    int contadorCaracteres = 0; // Contador caractres
    char almacenamientoLinea[3000]; // variable encargada de almacenar las lineas tiene capacidad hasta 3000
    // Recorre el archivo hasta que no se acabe
    while(!archivo.eof()){
      // Se ocupa getline para leer hasta encontrar cuando termine la linea
        archivo.getline(almacenamientoLinea, 3000);
        // Se crea un for para recorrer el archivo y contar las palabras
        for (int i = 0; i < strlen(almacenamientoLinea); i++){
            // el if cuenta los espacios vacios para determinar el fin de la palabra
          if ((almacenamientoLinea[i] ==  ' ' && i != 3000 && almacenamientoLinea[i+1] !=  ' ')){
            // Si se cumple la condicion aumenta el contador
            contadorPalabras++;
          }
        }
          // Fuera del for e if se realiza el calculo para contar los caracteres
          // por medio de srtlen calculando la longuitud y sumandolo con al contador
          contadorCaracteres = strlen(almacenamientoLinea) + contadorCaracteres;
          // Tambien fuera de for e if se aumenta el contador cuando se cumpla la condicion del getline
          contadorLineas++;
      }
    // Cada uno de los array tendra definida el contador para luego imprimirlo y usarlos por separado
      array[0] = contadorLineas; // Array 0 de lineas
      array[1] = contadorPalabras; // Array 1 de Palabras
      array[2] = contadorCaracteres; // Array 2 de Caracteres
      // Se imprime la cantidad de lineas palabras y caracteres segun sus contadores (ARRAY)
      cout << " - Lineas: " << array[0] << endl;
      cout << " - Palabras: " << array[1] << endl;
      cout << " - Caracteres: " << array[2] << endl;
      // Se retorna el array
      return array;
}

// Se crea esta funcion para calcular el tiempo de ejecucion del programa
float tiempoEjecucion(float tiempoInicial, float tiempoFinal) {
    // El tiempo de ejecucion es igual a la diferencia de tiempo final e incial
    // CLOCKS_PER_SEC permite obtener la cantidad de segundos
    float tiempoDiferencia = (tiempoFinal - tiempoInicial)/CLOCKS_PER_SEC;
    return tiempoDiferencia;
}

// El main permite la ejecucion del programa e implementacion de funciones
int main (int argc, char* argv[]) {
  // Si el argc es mayor o igual a uno se ingresan los archivos de texto
    if (argc >= 1) {
        cout << "Ingresando archivos de texto ..." << endl;
        cout << endl;
    }
    ifstream archivoTexto; // Almacenar los archivos de texto
    int *array; // se define un array
    int contadorTotalLineas = 0; // contador total de lineas
    int contadorTotalPalabras = 0; // contador total de palabras
    int contadorTotalCaracteres = 0; // contador total de CARACTERES
    float tiempoInicial, tiempoFinal; // variables foat del tiempo final e incial
    // Se imprime un mensaje que mostrara la informacion de cada archivo
    cout << " ------------------------------------------ " << endl;
    cout << " INFORMACION DE CADA ARCHIVO " << endl;
    cout << " ------------------------------------------ " << endl;
    // el tiempo inicial comienza a correr
    tiempoInicial = clock();
    // Se hace un for para poder realizar la operacion de las hebras y el resumen total
    for (int i=1; i < argc ; i++) {
      // se imprime el nombre del archivo
        cout << " NOMBRE ARCHIVO: " << argv[i] << endl;
        // El archivo se abre con el parametro
        archivoTexto.open(argv[i]);
        // Se ejecuta la funcion y se iguala al array para luego contar el total
        array = contarElementosArchivo(archivoTexto);
        // Contador total de lineas + contenido de las lineas por array 0
        contadorTotalLineas = contadorTotalLineas + array[0];
        // Contador total de palabras + contenido de las palabras por array 1
        contadorTotalPalabras = contadorTotalPalabras + array[1];
        // Contador total de caracteres + contenido de las caracteres por array 2
        contadorTotalCaracteres = contadorTotalCaracteres + array[2];
        // el achivo se cierra
        archivoTexto.close();
    }
    // se detiene el tiempo final
    tiempoFinal = clock();
    // Impresion del resumen total de lineas palabras y caracteres
    cout << endl;
    cout << " ------------------------------------------ " << endl;
    cout << " RESUMEN TOTAL " << endl;
    cout << " ------------------------------------------ " << endl;
    cout << " - Total Lineas: " << contadorTotalLineas << endl;
    cout << " - Total Palabras: " <<  contadorTotalPalabras << endl;
    cout << " - Total Caracteres: "  <<  contadorTotalCaracteres << endl;
    cout << endl;
    // Impresion que mostrara el tiempo de ejecucion del programa en segundos
    cout << " ------------------------------------------ " << endl;
    cout << " TIEMPO DE EJECUCION PROGRAMA: " << endl;
    cout << " ------------------------------------------ " << endl;
    cout << tiempoEjecucion(tiempoInicial, tiempoFinal) << " SEG" << endl;
    return 0;
}
